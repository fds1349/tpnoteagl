package part2;
//Étant donné que le TP1 du projet TER n'est pas terminé, ces tests ne testerons pas rééllement les méthodes, il faudra implementer correctement celles-ci mais le temps m'est comtpé.

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestJUnit {
	static Sujet s1,s2;
	static Groupe g1,g2;
	
	@BeforeAll
	static void init() {
		Sujet s1 = new Sujet(1,"Sujet Test 1");
		Sujet s2 = new Sujet(2,"Sujet Test 2");
		Groupe g1 = new Groupe(1,"Nom Groupe Test 1");
		Groupe g2 = new Groupe(2,"Nom Groupe Test 2");
	}
	
	
	@Test
	void testAffectationSujet() {
		s1.affecteGroupe(g1);
		assertEquals(s1.getGroupe(),g1.getSujet());
	}
	
}
