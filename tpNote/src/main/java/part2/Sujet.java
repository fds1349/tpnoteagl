package part2;

public class Sujet {
	private int id;
	private String titre;
	private Groupe g;
	
	public Sujet(int id,String titre) {
		this.id=id;
		this.titre=titre;
	}
	
	public int getId() {
		return id;
	}
	public String getTitre() {
		return titre;
	}
	public Groupe getGroupe() {
		return this.g;
	}
	
	public void affecteGroupe(Groupe g) {
		//On suppose que la méthode fonctionne
	}
	
	
}
