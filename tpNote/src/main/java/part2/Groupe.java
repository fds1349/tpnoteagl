package part2;

public class Groupe {
	private int id;
	private String nom;
	private Sujet sujet;
	
	public Groupe(int id,String nom) {
		this.id = id;
		this.nom= nom;
	}
	
	public int getId() {
		return id;
	}
	public String getNom() {
		return nom;
	}
	public Sujet getSujet() {
		return this.sujet;
	}
	
}
